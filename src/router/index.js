import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "../views/login"
import Layout from "../views/layout.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path:"/",
    name:"首页",
    redirect:"/project",
    component:Layout,
    meta:{
      requiresAuth:true
    },
    children:[
      {
        path:"project",
        name:"用户管理",
        component:() => import("../views/project"),
        meta:{
          requiresAuth:true
        },
      },
      {
        path:"project3",
        name:"发票详情",
        component:() => import("../views/project"),
        meta:{
          requiresAuth:true
        },
      },
      {
        path:"project4",
        name:"用户中心",
        component:() => import("../views/project"),
        meta:{
          requiresAuth:true
        },
      },
      {
        path:"project5",
        name:"数据统计",
        component:() => import("../views/project"),
        meta:{
          requiresAuth:true
        },
      },
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
